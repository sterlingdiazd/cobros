package com.lynx.charges;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.lynx.sales.bundle.db.Database;
import com.lynx.sales.bundle.provider.SalesContract;
import com.lynx.sales.bundle.provider.SalesContract.Sales;
import com.lynx.sales.bundle.provider.agent.AbstractProviderAgent;

public class ClientAgentProvider extends AbstractProviderAgent{

	public static final Uri CONTENT_URI = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "/users" ).build();
	
	private static final int ALL_CLIENTS  		 = 1;
	private static final int SINGLE_CLIENT	 	 = 2;
	private static final int SET_CLIENT_INFO 	 = 3;
	private static final int GET_CLIENT_INFO 	 = 4;
	
	private static UriMatcher matcher;
	
	static{
		matcher = new UriMatcher( UriMatcher.NO_MATCH );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "clients",   ALL_CLIENTS );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "clients/#", SINGLE_CLIENT ); 
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "clients/#/get/info", GET_CLIENT_INFO );
		matcher.addURI( Sales.PROVIDER_AUTHORITY, "clients/#/set/info", SET_CLIENT_INFO );
	}
	
	public ClientAgentProvider(Context context) {		
		super(context);
	}
	
	
	
	@Override
	public int delete(Uri uri, String selection, String[] seelctionArgs) {
		return 0;
	}
	
	

	@Override
	public String getType(Uri uri) {
		switch( matcher.match(uri) ){
		
		case ALL_CLIENTS :
			Log.e("*******************", "Resolving ... vnd.android.cursor.dir/vnd.com.lynx.sales.clients" );
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.users";
		case SINGLE_CLIENT:
			Log.e("*******************", "Resolving ... vnd.android.cursor.item/vnd.com.lynx.sales.clients" );
			return "vnd.android.cursor.item/vnd.com.lynx.sales.users";
		case GET_CLIENT_INFO:
			Log.e("*******************", "Resolving ... vnd.android.cursor.item/vnd.com.lynx.sales.users.clients.set_info" );
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.users.centers";		
		case SET_CLIENT_INFO:
			Log.e("*******************", "Resolving ... vnd.android.cursor.item/vnd.com.lynx.sales.users.clients.set_info" );
			return "vnd.android.cursor.dir/vnd.com.lynx.sales.users.centers";			
		}
		throw new UnsupportedOperationException( "Not supported URI" );
	}

	
	
	@Override
	public Uri insert(Uri uri, ContentValues values) {		
		SQLiteDatabase db = Database.getWritableDatabase( getContext() );

		String table = "";
		if( matcher.match(uri) == ALL_CLIENTS )
			table = "clients";
		else if( matcher.match(uri) == SET_CLIENT_INFO )
			table = "client_info";
		
		long id = db.insert( table, null, values);
		if( id > -1 ){			
			Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);			
			
			return insertedId;
		}
		return null;	
	}


	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArguments, String sortOrder) {
				
		//Log.e( "User Provider: *******", uri.toString() );
		
		String table = "";
		
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder(); 
		switch( matcher.match(uri) ){
		case SINGLE_CLIENT:
				table = "clients";
				builder.appendWhere( "id='"+uri.getLastPathSegment()+"'" );
				break;
		case GET_CLIENT_INFO:
				table = "client_info";				
				builder.appendWhere( "id_client='"+uri.getPathSegments().get(1)+"'" );
				break;
			default:
				break;
		}
		
		builder.setTables( table );
		return builder.query( Database.getReadableDatabase(getContext()), projection, selection, selectionArguments, null, null, sortOrder);
	}
	
	
	
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] seelctionArgs) {
		return 0;
	}


	@Override
	public boolean resolve(Uri uri){ 		
		return -1 < matcher.match(uri);
	}

	
}
