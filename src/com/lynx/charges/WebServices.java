package com.lynx.charges;

import java.util.ArrayList;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;

public class WebServices {

	
	public WebServices() {
		super();
	}

	public String assignValuesToQueryString(String queryString, ArrayList<NameValuePair> realValues)
	{
		String valuesAssigned = "";
		String [] strings = queryString.split(Pattern.quote("&"));
		ArrayList<String> listOfStrings = new ArrayList<String>();
		
		for(int x = 0; x < strings.length; x++)
		{
			listOfStrings.add(strings[x]);			
			String item  = listOfStrings.get(x);
			int indexOfLastCharacter = 0;
			if(item.indexOf('?') != -1){
				indexOfLastCharacter = item.indexOf('?');
				String itemEmpty = item.substring(0, indexOfLastCharacter);
				String itemFull = itemEmpty + realValues.get(x).getValue();				
				valuesAssigned += itemFull + "&";
			} else {
				valuesAssigned += item + "&";
			}
			
						
		}
		int lastAnd = valuesAssigned.lastIndexOf('&');
		String finalQueryString = valuesAssigned.substring(0, lastAnd);
		return finalQueryString;		
	}
}
