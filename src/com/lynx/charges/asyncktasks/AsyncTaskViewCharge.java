package com.lynx.charges.asyncktasks;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.lyxar.envelope.Broker;
import org.lyxar.envelope.Service;
import org.lyxar.envelope.sp.BrokerService;
import org.lyxar.envelope.sp.BrokersServices;

import com.lynx.charges.Keys;
import com.lynx.charges.URLManager;
import com.lynx.charges.WebServices;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class AsyncTaskViewCharge extends AsyncTask<String,Integer,Boolean> {

    public ProgressDialog progressDialog;
    private Context context;
	private Service service;
	private Broker broker;
	private  HttpGet httpGet;
	private String sapResponse;
	private String startDate, endDate, society, seller, client, cashRegister, checkNum, historical;
	private JSONArray  deposits, checks;
	private JSONObject jObject;
	
	public AsyncTaskViewCharge(Context context) {
		super();
		this.context = context;
		
		BrokerService brokerService = BrokersServices.defaultBrokerService(context);
		broker = brokerService.getBrokerById( "SAP" );
		service = broker.getService( "view_charges" );
	}

	@Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Enviando");
        progressDialog.show();
    }

    protected Boolean doInBackground(String... params) {

        boolean didDataCame = false;

        try
        {
            ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
            parametros.add(new BasicNameValuePair(Keys.START_DATE, params[0]));
            parametros.add(new BasicNameValuePair(Keys.END_DATE, params[1]));
            parametros.add(new BasicNameValuePair(Keys.SOCIETY, params[2]));
            parametros.add(new BasicNameValuePair(Keys.CASH_REGISTER, params[3]));
            parametros.add(new BasicNameValuePair(Keys.SELLER, params[4]));
            parametros.add(new BasicNameValuePair(Keys.CLIENT, params[5]));
            parametros.add(new BasicNameValuePair(Keys.CHECK_NO, params[6]));
            parametros.add(new BasicNameValuePair(Keys.HISTORICAL, params[7]));
          
            WebServices webServices = new WebServices();
            String queryStringWithValuesAssigned = webServices.assignValuesToQueryString(service.getQueryString(), parametros);
            String url = broker.getHost() + service.getPath() + service.getURLString() + queryStringWithValuesAssigned;
            
            URLManager urlManager = new URLManager();
            String urlPurified = urlManager.purifyURL(url);
            
            try {
           	 httpGet = new HttpGet(urlPurified);
           	 
			} catch (Exception e) {
				e.printStackTrace();
			}
           
           httpGet.setHeader("content-type", "application/json");

           HttpClient httpClient = new DefaultHttpClient();
           HttpResponse resp = null;
           try {
               resp = httpClient.execute(httpGet);
           } catch (IOException e) {
               e.printStackTrace();
           }
           HttpEntity httpEntity = resp.getEntity();
           String respStr = EntityUtils.toString(httpEntity);
           jObject = new JSONObject(respStr);
           startDate = jObject.getString(Keys.START_DATE);
           endDate = jObject.getString(Keys.END_DATE);
           society = jObject.getString(Keys.SOCIETY);
           cashRegister = jObject.getString(Keys.CASH_REGISTER);
           seller = jObject.getString(Keys.SELLER);
           client = jObject.getString(Keys.CLIENT);
           checkNum = jObject.getString(Keys.CHECK_NO);
           historical = jObject.getString(Keys.HISTORICAL);
           deposits = jObject.getJSONArray(Keys.DEPOSITS);
           checks = jObject.getJSONArray(Keys.CHECKS);
        }
        catch(Exception ex)
        {
            Log.e("ServicioRest", "Error!", ex);
        }
        return didDataCame;
    }

    public JSONObject getjObject() {
		return jObject;
	}

	public void setjObject(JSONObject jObject) {
		this.jObject = jObject;
	}

	protected void onPostExecute(Boolean result) 
    {
        progressDialog.dismiss(); 
        Toast.makeText(context, sapResponse, Toast.LENGTH_SHORT).show();
    }
}