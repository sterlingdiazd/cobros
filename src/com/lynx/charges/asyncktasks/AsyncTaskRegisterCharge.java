package com.lynx.charges.asyncktasks;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.lyxar.envelope.Broker;
import org.lyxar.envelope.Service;
import org.lyxar.envelope.sp.BrokerService;
import org.lyxar.envelope.sp.BrokersServices;

import com.lynx.charges.ChargeActivity;
import com.lynx.charges.Keys;
import com.lynx.charges.URLManager;
import com.lynx.charges.WebServices;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.WebSettings;
import android.widget.Toast;

public class AsyncTaskRegisterCharge extends AsyncTask<String,Integer,Boolean> {

    private ProgressDialog progressDialog;
    private Context context;
	private Service service;
	private Broker broker;
	private  HttpGet httpGet;
	private String sapResponse;
	private boolean isRegistered;

	public AsyncTaskRegisterCharge(Context context) {
		super();
		this.context = context;
		
		BrokerService brokerService = BrokersServices.defaultBrokerService(context);
		broker = brokerService.getBrokerById( "SAP" );
		service = broker.getService( "/register_charge" );
	}

	@Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Enviando");
        progressDialog.show();
    }

    protected Boolean doInBackground(String... params) {

        isRegistered = false;

        try
        {
            ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
            parametros.add(new BasicNameValuePair(Keys.ACTION, 			params[0]));
            parametros.add(new BasicNameValuePair(Keys.TYPE, 			params[1]));
            parametros.add(new BasicNameValuePair(Keys.AMOUNT, 			params[2]));
            parametros.add(new BasicNameValuePair(Keys.CLIENT, 			params[3]));
            parametros.add(new BasicNameValuePair(Keys.SELLER, 			params[4]));
            parametros.add(new BasicNameValuePair(Keys.SOCIETY, 		params[5]));
            parametros.add(new BasicNameValuePair(Keys.CASH_REGISTER, 	params[6]));
            parametros.add(new BasicNameValuePair(Keys.USER, 			params[7]));
            parametros.add(new BasicNameValuePair(Keys.CHECK_NO, 		params[8]));
            parametros.add(new BasicNameValuePair(Keys.INVOICE, 		params[9]));
            parametros.add(new BasicNameValuePair(Keys.BANK, 			params[10]));
            parametros.add(new BasicNameValuePair(Keys.BANK_INITIALS, 	params[11]));
            
            WebServices webServices = new WebServices();
            String queryStringWithValuesAssigned = webServices.assignValuesToQueryString(service.getQueryString(), parametros);
            String url = broker.getHost() + service.getPath() + service.getURLString() + '?' + queryStringWithValuesAssigned;
            
            URLManager urlManager = new URLManager();
            String urlPurified = urlManager.purifyURL(url);
           
            try {
            	 httpGet = new HttpGet(urlPurified);
            	 
			} catch (Exception e) {
				e.printStackTrace();
			}
            
            httpGet.setHeader("content-type", "application/json");

            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse resp = null;
            try {
                resp = httpClient.execute(httpGet);
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpEntity httpEntity = resp.getEntity();
            String respStr = EntityUtils.toString(httpEntity);
            JSONObject jObject=new JSONObject(respStr);
            sapResponse = jObject.getString("message");
            
            if(sapResponse.equalsIgnoreCase("Los registros han sido salvados"))
			{
            	isRegistered = true;
			} else {
				Log.e("ERROR", sapResponse);
			}
        }
        catch(Exception ex)
        {
            Log.e("ServicioRest", "Error!", ex);
            ex.printStackTrace();
        }
        // Return false if it was not registered
        return isRegistered;
    }


    protected void onPostExecute(Boolean result) {

       
    	progressDialog.dismiss();
    	Toast.makeText(context, sapResponse, Toast.LENGTH_SHORT).show();
        
        
        if (result)
        {
        	ChargeActivity.clearFields();
        	/*
            Toast.makeText(context, "Insertado OK.", Toast.LENGTH_LONG ).show();
            configuration.getPrefsEditor().putBoolean("isRegistered", true);
            configuration.getPrefsEditor().commit();
            */
        }
        else
        {
            
        }
        
        /*
        DashboardFragment dashboardFragment= new DashboardFragment(context);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, dashboardFragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
        */
    	
    	
    	
    }

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}
}