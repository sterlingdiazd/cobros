package com.lynx.charges;

import java.io.Serializable;

import android.text.TextUtils;

@SuppressWarnings("serial")
public class CenterEntity implements Serializable {

	private String name;
	private String store;

	public CenterEntity() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}
	
	public String toString() {
		return name + (!TextUtils.isEmpty( store ) ? ":"+store : "" );
	};
}
