package com.lynx.charges;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.lynx.sales.bundle.provider.SalesContract;

public class ClientRetrieval {

	public static ClientEntity getClient( Context cxt, String clientID ){
		
		ClientEntity client = new ClientEntity();
		
		ContentResolver resolver = cxt.getContentResolver();
		Uri uri = SalesContract.Sales.CONTENT_URI.buildUpon().appendPath( "clients" ).appendPath(  clientID ).build();
		
		Cursor cursor = resolver.query(uri, new String[]{ "id","name" }, null,null, null);
		if( null != cursor && cursor.moveToFirst() ){
			do{
				client.setId( cursor.getString( cursor.getColumnIndexOrThrow( "id" ) ) );
				client.setName( cursor.getString( cursor.getColumnIndexOrThrow( "name" ) ) );
			}while( cursor.moveToNext() );
		}	
		cursor.close();
		return client;
	}
}

