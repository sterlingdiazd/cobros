package com.lynx.charges;

public class Keys {

	public static final String ACTION = "action";
	public static final String START_DATE = "start_date";
	public static final String END_DATE = "end_date";
	public static final String SOCIETY = "society";

	public static final String SELLER = "seller";
	public static final String CLIENT = "client";
	public static final String CASH_REGISTER = "cash_register";

	public static final String CHECK_NO = "check_no";
	
	public static final String CHARGES = "charges";
	public static final String DEPOSITS = "deposits";
	public static final String CASH = "Cash";
	public static final String CHECKS = "Checks";
	public static final String HISTORICAL = "historical";
	
	public static final String ID = "id";
	public static final String DATE = "date";
	public static final String TYPE = "type";
	public static final String AMOUNT = "amount";
	public static final String USER = "user";
	public static final String BANK = "bank";
	public static final String BANK_INITIALS = "bank_initials";
	public static final String INVOICE = "invoice";
	
}
