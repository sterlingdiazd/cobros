package com.lynx.charges;

import java.io.Serializable;
import java.util.List;

@SuppressWarnings("serial")
public class UserEntity implements Serializable {

	private String id;
	private String name;
	private String salesOrganization;
	private String docType;
	private List<CenterEntity> centers;
	private CenterEntity defaultCenter;

	public UserEntity() {
	}

	public UserEntity(String id, String name, String organization) {
		this.id = id;
		this.name = name;
		this.salesOrganization = organization;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSalesOrganization() {
		return salesOrganization;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public List<CenterEntity> getCenters() {
		return centers;
	}

	public void setCenters(List<CenterEntity> centers) {
		this.centers = centers;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSalesOrganization(String salesOrganization) {
		this.salesOrganization = salesOrganization;
	}

	public CenterEntity getDefaultCenter() {
		return defaultCenter;
	}

	public void setDefaultCenter(CenterEntity defaultCenter) {
		this.defaultCenter = defaultCenter;
	}

	
	@Override
	public String toString() {
		return "{ id:"+id+",name:"+name+" }";
	}
}
