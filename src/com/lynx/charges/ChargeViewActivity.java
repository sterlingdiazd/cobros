package com.lynx.charges;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lyxar.envelope.Broker;
import org.lyxar.envelope.Service;
import org.lyxar.envelope.sp.BrokerService;
import org.lyxar.envelope.sp.BrokersServices;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.lynx.charges.adapters.AdapterChecks;
import com.lynx.charges.adapters.AdapterDeposits;
import com.lynx.sales.bundle.app.LynxApplication;
import com.lynxsales.printer.AsyncTaskDiscover;
import com.lynxsales.printer.entities.Charge;
import com.lynxsales.printer.entities.UserEntity;

public class ChargeViewActivity extends Activity implements OnItemClickListener {
	
	private Context context;
	private TabHost tabs;
	private ListView listViewCheck, listViewDeposits, listViewHistorical;
	private AdapterChecks adapterChecks;
	private AdapterDeposits adapterDeposits;
	private TextView textViewParameterStartDate, textViewParameterEndDate, textViewParameterSociety, textViewParameterSeller,
	textViewParameterClient, textViewParameterCashRegister, textViewInvoice;
	private String startDate, endDate, society, seller, client, invoice, checkNum, cashRegister,  historical;
	private String action;
	private static AlertDialog.Builder	alertDialogBuilder;
	private boolean print_confirmation;
	private SharedPreferences prefs;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.charge_visualization);
		context = ChargeViewActivity.this;
		
		@SuppressWarnings("unchecked")
		Intent parameters = getIntent();
		action = parameters.getStringExtra(Keys.ACTION);
		startDate = parameters.getStringExtra(Keys.START_DATE);
		endDate = parameters.getStringExtra(Keys.END_DATE);
		society = parameters.getStringExtra(Keys.SOCIETY);
		cashRegister = parameters.getStringExtra(Keys.CASH_REGISTER);		
		seller = parameters.getStringExtra(Keys.SELLER);
		client = parameters.getStringExtra(Keys.CLIENT);
		invoice = parameters.getStringExtra(Keys.INVOICE);
		checkNum = parameters.getStringExtra(Keys.CHECK_NO);
		historical = parameters.getStringExtra(Keys.HISTORICAL);
		
		AsyncTaskViewCharge asyncTaskViewCharge = new AsyncTaskViewCharge(context);
		asyncTaskViewCharge.execute(action, startDate, endDate, society,  cashRegister, seller, client,invoice, checkNum, historical);
		
	}

	private void configureComponents() 
	{
		tabs=(TabHost)findViewById(android.R.id.tabhost);
		listViewCheck = (ListView) findViewById(R.id.listViewCheck);
		listViewDeposits = (ListView) findViewById(R.id.listViewDeposits);
		listViewHistorical = (ListView) findViewById(R.id.listViewHistorical);
		
		textViewParameterStartDate = (TextView) findViewById(R.id.textViewParameterStartDate);
		textViewParameterEndDate = (TextView) findViewById(R.id.textViewParameterEndDate);
		textViewParameterSociety = (TextView) findViewById(R.id.textViewParameterSociety);
		textViewParameterSeller = (TextView) findViewById(R.id.textViewParameterSeller);
		textViewParameterClient = (TextView) findViewById(R.id.textViewParameterClient);
		textViewParameterCashRegister = (TextView) findViewById(R.id.textViewParameterCashRegister);
		
		Utility utility = new Utility();
		
		textViewParameterStartDate.setText(utility.separateDateByHiphen(startDate));
		textViewParameterEndDate.setText(utility.separateDateByHiphen(endDate));
		textViewParameterSociety.setText(society);
		textViewParameterSeller.setText(seller);
		textViewParameterClient.setText(client);
		textViewParameterCashRegister.setText(cashRegister);
		
	} 



	private void configureTabs(JSONObject jsonObject)
	{
		Resources res = getResources();
		tabs.setup();
		
		TabHost.TabSpec spec=tabs.newTabSpec(Keys.CHECKS);
		spec.setContent(R.id.tabCheck);
		spec.setIndicator(Keys.CHECKS, res.getDrawable(android.R.drawable.ic_btn_speak_now));
		JSONArray checks;
		try {
				checks = jsonObject.getJSONArray( (Keys.CHECKS).toLowerCase());
				adapterChecks = new AdapterChecks(ChargeViewActivity.this, checks);
				listViewCheck = (ListView) findViewById( R.id.listViewCheck );
				listViewCheck.setAdapter(adapterChecks);
				listViewCheck.refreshDrawableState();
				listViewCheck.setOnItemClickListener(this);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		tabs.addTab(spec);
		
		spec=tabs.newTabSpec(Keys.CASH);
		spec.setContent(R.id.tabDeposits);
		spec.setIndicator(Keys.DEPOSITS, res.getDrawable(android.R.drawable.ic_btn_speak_now));
		JSONArray deposits;
		
		
		
		try {
				deposits = jsonObject.getJSONArray(Keys.DEPOSITS);
				adapterDeposits = new AdapterDeposits(context, deposits);
				listViewCheck = (ListView) findViewById( R.id.listViewDeposits );
				listViewCheck.setAdapter(adapterDeposits);
				listViewCheck.refreshDrawableState();
				listViewCheck.setOnItemClickListener(this);
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		tabs.addTab(spec);
		
		spec=tabs.newTabSpec(Keys.HISTORICAL);
		spec.setContent(R.id.tabHistorical);
		spec.setIndicator(Keys.HISTORICAL, res.getDrawable(android.R.drawable.ic_btn_speak_now));
		JSONArray historical;
		
		try {
				historical = jsonObject.getJSONArray(Keys.HISTORICAL);
				adapterChecks = new AdapterChecks(ChargeViewActivity.this, historical);
				listViewCheck = (ListView) findViewById( R.id.listViewHistorical );
				listViewCheck.setAdapter(adapterChecks);
				listViewCheck.refreshDrawableState();
				listViewCheck.setOnItemClickListener(this);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		tabs.addTab(spec);
		 
		tabs.setCurrentTab(0);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
	{		
		printConfirmationDialog().show();
		
		if(print_confirmation)
		{
			Toast.makeText(context, "Printing", Toast.LENGTH_LONG).show();
			JSONObject json_charge = (JSONObject) parent.getItemAtPosition(position);
			Charge charge = new Charge(json_charge);
			
			ArrayList<Charge> charges = new ArrayList<Charge>();
			charges.add(charge);
			ImageView print = (ImageView) view.findViewById(R.id.imageViewPrintCharge);
			
			prefs = PreferenceManager.getDefaultSharedPreferences(this);
			boolean isPriceEnabled = prefs.getBoolean("key_price_enabled", false);
			
			AsyncTaskDiscover asyncTaskDiscover = new AsyncTaskDiscover(this, charges, isPriceEnabled, isPriceEnabled, isPriceEnabled);
			asyncTaskDiscover.execute();
		}
		
		
		
	}
	
	private Dialog printConfirmationDialog()
	{
		alertDialogBuilder = new AlertDialog.Builder( context );
		alertDialogBuilder.setTitle( context.getResources().getString( R.string.print_confirmation) );
		alertDialogBuilder.setIcon( context.getResources().getDrawable( R.drawable.bg_pick ) );
		alertDialogBuilder.setMessage("Do want to print document?");
		
		alertDialogBuilder.setPositiveButton( "Yes", new DialogInterface.OnClickListener()
		{
			public void onClick( DialogInterface dialog, int which )
			{
				print_confirmation = true;
				dialog.cancel();
			}
		} );
		
		alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener()
		{
			public void onClick( DialogInterface dialog, int which )
			{
				print_confirmation = false;
				dialog.cancel();
			}
		} );
		
		
		return alertDialogBuilder.create();
	}
	
	public class AsyncTaskViewCharge extends AsyncTask<String,Integer,Boolean> {

    public ProgressDialog progressDialog;
    private Context context;
	private Service service;
	private Broker broker;
	private  HttpGet httpGet;
	private String sapResponse;
	private String startDate, endDate, society, seller, client, cashRegister, invoice, historical;
	private JSONArray  deposits, checks;
	private JSONObject jObject;
	
	public AsyncTaskViewCharge(Context context) {
		super();
		this.context = context;
		
		BrokerService brokerService = BrokersServices.defaultBrokerService(context);
		broker = brokerService.getBrokerById( "SAP" );
		service = broker.getService( "/view_charges" );
	}

	@Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Enviando");
        progressDialog.show();
    }

    protected Boolean doInBackground(String... params) {

        boolean didDataCame = false;

        try
        {
            ArrayList<NameValuePair> parametros = new ArrayList<NameValuePair>();
            parametros.add(new BasicNameValuePair(Keys.ACTION, 			params[0]));
            parametros.add(new BasicNameValuePair(Keys.START_DATE, 		params[1]));
            parametros.add(new BasicNameValuePair(Keys.END_DATE, 		params[2]));
            parametros.add(new BasicNameValuePair(Keys.SOCIETY, 		params[3]));
            parametros.add(new BasicNameValuePair(Keys.CASH_REGISTER, 	params[4]));
            parametros.add(new BasicNameValuePair(Keys.SELLER, 			params[5]));
            parametros.add(new BasicNameValuePair(Keys.CLIENT, 			params[6]));
            parametros.add(new BasicNameValuePair(Keys.INVOICE, 		params[7]));
            parametros.add(new BasicNameValuePair(Keys.CHECK_NO, 		params[8]));
            parametros.add(new BasicNameValuePair(Keys.HISTORICAL, 		params[9]));
          
            WebServices webServices = new WebServices();
            String queryStringWithValuesAssigned = webServices.assignValuesToQueryString(service.getQueryString(), parametros);
            String url = broker.getHost() + service.getPath() + service.getURLString() + '?' + queryStringWithValuesAssigned;
            
            URLManager urlManager = new URLManager();
            String urlPurified = urlManager.purifyURL(url);
            
            try {
           	 httpGet = new HttpGet(urlPurified);
           	 
			} catch (Exception e) {
				e.printStackTrace();
			}
           
           httpGet.setHeader("content-type", "application/json");

           HttpClient httpClient = new DefaultHttpClient();
           HttpResponse resp = null;
           try {
               resp = httpClient.execute(httpGet);
           } catch (IOException e) {
               e.printStackTrace();
           }
           HttpEntity httpEntity = resp.getEntity();
           String respStr = EntityUtils.toString(httpEntity);
           jObject = new JSONObject(respStr);
           if(jObject != null)
           {
        	   didDataCame = true;
           }
        }
        catch(Exception ex)
        {
            Log.e("ServicioRest", "Error!", ex);
        }
        return didDataCame;
    }

    public JSONObject getjObject() {
		return jObject;
	}

	public void setjObject(JSONObject jObject) {
		this.jObject = jObject;
	}

	protected void onPostExecute(Boolean result) 
    {
        progressDialog.dismiss(); 
        configureComponents();
		configureTabs(getjObject());
    }
}
	
}
