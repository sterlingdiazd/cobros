package com.lynx.charges;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

public class DatePickerFragment extends DialogFragment {

	private int year;
	private int month;
	private int day;
	private OnDateSetListener onDateSetListener;
	private int idButtonSelected;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		final Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		int month = c.get(Calendar.MONTH);
		int day = c.get(Calendar.DAY_OF_MONTH);

		return new DatePickerDialog(getActivity(), onDateSetListener, year,
				month, day);
	}

	public void setOnDateSetListener(OnDateSetListener onDateSetListener) {
		this.onDateSetListener = onDateSetListener;
	}

	public OnDateSetListener getOnDateSetListener() {
		return onDateSetListener;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}

	public void setIdButtonSelected(int id) {
		this.idButtonSelected = id;

	}

	public int getIdButtonSelected() {
		return idButtonSelected;
	}

}
