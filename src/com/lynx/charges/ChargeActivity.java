package com.lynx.charges;

import java.util.ArrayList;

import org.lyxar.envelope.CompletionHandler;
import org.lyxar.envelope.InvariantChecker;
import org.lyxar.envelope.sp.BrokersServices;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.lynx.charges.asyncktasks.AsyncTaskRegisterCharge;
import com.lynx.sales.bundle.dto.model.UserRetrieval;

public class ChargeActivity extends FragmentActivity implements
		OnClickListener, OnDateSetListener {

	private Context context;
	private TabHost tabs;
	private String selectedTab;
	public static final String TABVIEW = "TABVIEW";
	public static final String TABREGISTER = "TABREGISTER";
	private EditText editTextSociety_View;
	private EditText editTextCashRegister_View;
	private EditText editTextSeller_View;
	private EditText editTextClient_View;
	private EditText editTextCheckNum_View;
	private EditText editTextInvoice_View;
	private ToggleButton toggleButtonBringHistorical_View;
	private Button buttonSend_View;
	private Button buttonSetStartDate;
	private Button buttonSetEndDate;

	public static Spinner spinnerType;
	public static EditText editTextStartDate;
	public static EditText editTextEndDate;
	public static EditText editTextAmount;
	public static EditText editTextClient_Register;
	public static EditText editTextSeller_Register;
	public static EditText editTextSociety_Register;
	public static EditText editTextCashRegister_Register;
	public static EditText editTextUser;
	public static EditText editTextCheckNum_Register;
	public static EditText editTextBank;
	public static EditText editTextBankInitials;

	private Button buttonSend_Register;

	private DatePickerFragment datePickerFragment;
	private String startDate, endDate, society, seller, client, cashRegister,
			checkNum, invoice, historical;
	private String type, amount, user, bank, bankInitials;
	private Boolean isHistoricalActivated;
	private Utility utility;
	private String clientID;
	private String action;
	public static final String EXTRA_CLIENT_ID = "com.lynx.sales.activities.bprocess.BillActivity.EXTRA_CLIENT_ID";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		context = ChargeActivity.this;

		prepareServices();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void prepareServices() {
		InvariantChecker checker = BrokersServices.defaultInvariantChecker();
		checker.resolve(this, new CompletionHandler() {
			@Override
			public void onCompleted() {
				chargeValuesForKnownValues();
			}
		});
	}

	private void configureComponents(UserEntity userEntity, ClientEntity client) {

		utility = new Utility();
		tabs = (TabHost) findViewById(android.R.id.tabhost);

		buttonSetStartDate = (Button) findViewById(R.id.buttonSetStartDate);
		buttonSetEndDate = (Button) findViewById(R.id.buttonSetEndDate);
		editTextStartDate = (EditText) findViewById(R.id.editTextStartDate);
		editTextEndDate = (EditText) findViewById(R.id.editTextEndDate);
		editTextSociety_View = (EditText) findViewById(R.id.editTextSociety_View);
		editTextCashRegister_View = (EditText) findViewById(R.id.editTextCashRegister_View);
		editTextSeller_View = (EditText) findViewById(R.id.editTextSeller_View);
		editTextClient_View = (EditText) findViewById(R.id.editTextClient_View);
		editTextCheckNum_View = (EditText) findViewById(R.id.editTextCheckNum_View);
		editTextInvoice_View = (EditText) findViewById(R.id.editTextInvoice_View);
		toggleButtonBringHistorical_View = (ToggleButton) findViewById(R.id.toggleButtonBringHistorical_View);
		buttonSend_View = (Button) findViewById(R.id.buttonSend_View);

		editTextStartDate.setOnClickListener(this);
		editTextEndDate.setOnClickListener(this);

		editTextSociety_View.setText(userEntity.getSalesOrganization());
		editTextSociety_View.setEnabled(false);
		editTextSeller_View.setText(userEntity.getId());
		editTextSeller_View.setEnabled(false);
		
		//editTextClient_View.setText(client.getId());
		//editTextClient_View.setEnabled(false);
		
		editTextCashRegister_View.setText("0001");
		editTextCashRegister_View.setEnabled(false);

		buttonSetStartDate.setOnClickListener(this);
		buttonSetEndDate.setOnClickListener(this);
		buttonSend_View.setOnClickListener(this);

		spinnerType = (Spinner) findViewById(R.id.spinnerType);

		editTextAmount = (EditText) findViewById(R.id.editTextAmount);
		editTextClient_Register = (EditText) findViewById(R.id.editTextClient_Register);
		editTextSeller_Register = (EditText) findViewById(R.id.editTextSeller_Register);
		editTextSociety_Register = (EditText) findViewById(R.id.editTextSociety_Register);
		editTextCashRegister_Register = (EditText) findViewById(R.id.editTextCashRegister_Register);
		editTextUser = (EditText) findViewById(R.id.editTextUser);
		editTextCheckNum_Register = (EditText) findViewById(R.id.editTextCheckNum_Register);
		editTextBank = (EditText) findViewById(R.id.editTextBank);
		editTextBankInitials = (EditText) findViewById(R.id.editTextBankInitials);

		editTextSeller_Register.setText(userEntity.getId());
		editTextSociety_Register.setText(userEntity.getSalesOrganization());
		//editTextClient_Register.setText(clientID);
		editTextSeller_Register.setEnabled(false);
		editTextSociety_Register.setEnabled(false);
		editTextClient_Register.setEnabled(false);
		editTextCashRegister_Register.setText("0001");
		editTextCashRegister_Register.setEnabled(false);
		editTextUser.setText(userEntity.getName());
		editTextUser.setEnabled(false);

		buttonSend_Register = (Button) findViewById(R.id.buttonSend_Register);
		buttonSend_Register.setOnClickListener(this);

		ArrayList<String> chargeTypes = new ArrayList<String>();
		chargeTypes.add(this.getResources().getString(
				R.string.charge_type_cheque));
		chargeTypes.add(this.getResources().getString(
				R.string.charge_type_cheque_corbata));
		chargeTypes.add(this.getResources().getString(
				R.string.charge_type_efectivo));
		ArrayAdapter<String> arrayAdapterAccountTypes = new ArrayAdapter<String>(
				this.context, android.R.layout.simple_spinner_dropdown_item,
				chargeTypes);
		spinnerType.setAdapter(arrayAdapterAccountTypes);

		try {
			configureTabs();
		} catch (Exception e) {
			Log.e("error", "configureTabs");
			e.printStackTrace();
		}

		try {
			configureTabChanges();
		} catch (Exception e) {
			Log.e("error", "configureTabChanges");
			e.printStackTrace();
		}
	}

	private void chargeValuesForKnownValues() {

		setContentView(R.layout.activity_main);
		//UserEntity user = UserRetrieval.getUser(context);
		UserEntity user = new UserEntity();
		user.setId("102");
		user.setName("Leonardo");
		user.setSalesOrganization("MA01");
		user.setDocType("VM");
		//clientID = /*getBussinessClient()*/  "0000101248";
		//ClientEntity client = ClientRetrieval.getClient(context, clientID);
		ClientEntity client = new ClientEntity();
		client.setId("0000101248");
		client.setName("Cliente1");
		configureComponents(user, client);
	}

	private String getBussinessClient() {
		if (getIntent().getExtras() != null) {
			Intent intent = getIntent();
			Bundle bundle = intent.getExtras();
			clientID = bundle.getString(EXTRA_CLIENT_ID);
		}
		return clientID;
	}

	public void configureTabs() {
		Resources res = getResources();
		tabs.setup();

		TabHost.TabSpec spec = tabs.newTabSpec(TABVIEW);
		spec.setContent(R.id.tabView);
		spec.setIndicator("View",
				res.getDrawable(android.R.drawable.ic_btn_speak_now));
		tabs.addTab(spec);

		spec = tabs.newTabSpec(TABREGISTER);
		spec.setContent(R.id.tabRegister);
		spec.setIndicator("Register",
				res.getDrawable(android.R.drawable.ic_dialog_map));
		tabs.addTab(spec);

		tabs.setCurrentTab(0);
	}

	public void configureTabChanges() {
		tabs.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				selectedTab = tabId;
				if (tabId.equalsIgnoreCase(TABVIEW)) {
					// Toast.makeText(context, "Tabview",
					// Toast.LENGTH_SHORT).show();
				} else if (tabId.equalsIgnoreCase(TABREGISTER)) {
					// Toast.makeText(context, "Tab reg",
					// Toast.LENGTH_SHORT).show();
				}
			}
		});

	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.buttonSend_Register) {
			action = "1";
			type = spinnerType.getSelectedItem().toString();
			amount = editTextAmount.getText().toString();
			client = editTextClient_Register.getText().toString();
			seller = editTextSeller_Register.getText().toString();
			society = editTextSociety_Register.getText().toString();
			cashRegister = editTextCashRegister_Register.getText().toString();
			user = editTextUser.getText().toString();
			checkNum = editTextCheckNum_Register.getText().toString();
			invoice = editTextInvoice_View.getText().toString();
			bank = editTextBank.getText().toString();
			bankInitials = editTextBankInitials.getText().toString();
			AsyncTaskRegisterCharge asyncTaskRegisterCharge = new AsyncTaskRegisterCharge(
					context);
			asyncTaskRegisterCharge.execute(action, type, amount, client, seller,
					society, cashRegister, user, checkNum, invoice, bank,
					bankInitials);
		} else if (id == R.id.buttonSetStartDate) {
			showDatePickerDialog(buttonSetStartDate);
		} else if (id == R.id.buttonSetEndDate) {
			showDatePickerDialog(buttonSetEndDate);
		} else if (id == R.id.editTextStartDate) {
			showDatePickerDialog(buttonSetStartDate);
		} else if (id == R.id.editTextEndDate) {
			showDatePickerDialog(buttonSetEndDate);
		} else if (id == R.id.buttonSend_View) {
			
			if (editTextStartDate.getText().toString().length() <= 0) {
				Toast.makeText(context, "Debe proveer una fecha inicial",
						Toast.LENGTH_SHORT).show();
				return;
			}
			if (editTextEndDate.getText().toString().length() <= 0) {
				Toast.makeText(context, "Debe proveer una fecha final",
						Toast.LENGTH_SHORT).show();
				return;
			}
			action = "2";
			startDate = utility.getRidOfHyphenInDate(editTextStartDate
					.getText().toString());
			endDate = utility.getRidOfHyphenInDate(editTextEndDate.getText()
					.toString());
			society = editTextSociety_View.getText().toString();
			cashRegister = editTextCashRegister_View.getText().toString();
			seller = editTextSeller_View.getText().toString();
			client = editTextClient_View.getText().toString();
			checkNum = editTextCheckNum_View.getText().toString();
			invoice = editTextInvoice_View.getText().toString();

			if (toggleButtonBringHistorical_View.isChecked()) {
				historical = "X";
			} else {
				historical = "";
			}
			ArrayList<String> parameters = new ArrayList<String>();
			parameters.add(action);
			parameters.add(startDate);
			parameters.add(endDate);
			parameters.add(society);
			parameters.add(cashRegister);
			parameters.add(seller);
			parameters.add(client);
			parameters.add(invoice);
			parameters.add(checkNum);
			parameters.add(historical);
			Intent intent = new Intent(context, ChargeViewActivity.class);
			intent.putExtra(Keys.ACTION, action);
			intent.putExtra(Keys.START_DATE, startDate);
			intent.putExtra(Keys.END_DATE, endDate);
			intent.putExtra(Keys.SOCIETY, society);
			intent.putExtra(Keys.CASH_REGISTER, cashRegister);
			intent.putExtra(Keys.SELLER, seller);
			intent.putExtra(Keys.CLIENT, client);
			intent.putExtra(Keys.INVOICE, client);
			intent.putExtra(Keys.CHECK_NO, checkNum);
			intent.putExtra(Keys.HISTORICAL, historical);
			startActivity(intent);
		} else {
		}

	}

	public void showDatePickerDialog(Button buttonSelected) {
		datePickerFragment = new DatePickerFragment();
		datePickerFragment.setIdButtonSelected(buttonSelected.getId());
		datePickerFragment.show(getSupportFragmentManager(), "datepicker");
		datePickerFragment.setOnDateSetListener(this);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {

		String date = utility.createDateWithHyphen(year, month + 1, day);

		int idButtonSelected = datePickerFragment.getIdButtonSelected();
		if (idButtonSelected == R.id.buttonSetStartDate) {
			buttonSetStartDate.setVisibility(Button.GONE);
			editTextStartDate.setVisibility(EditText.VISIBLE);
			editTextStartDate.setBackgroundDrawable(getResources().getDrawable(
					R.drawable.selector_input_text));
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT);
			editTextStartDate.setLayoutParams(params);
			editTextStartDate.setText(date);
		} else if (idButtonSelected == R.id.buttonSetEndDate) {
			buttonSetEndDate.setVisibility(Button.GONE);
			editTextEndDate.setVisibility(EditText.VISIBLE);
			editTextEndDate.setText(date);
		}

	}

	public static void clearFields() {

		editTextAmount.setText("");
		editTextClient_Register.setText("");
		editTextSeller_Register.setText("");
		editTextSociety_Register.setText("");
		editTextCashRegister_Register.setText("");
		editTextUser.setText("");
		editTextCheckNum_Register.setText("");
		editTextBank.setText("");
		editTextBankInitials.setText("");

	}
}
