package com.lynx.charges.adapters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.lynx.charges.Keys;
import com.lynx.charges.R;
import com.lynx.charges.Utility;


public class AdapterDeposits extends BaseAdapter {

    private Context context;
    private JSONArray deposits;
    private TextView  textViewCashRegister, textViewDate, textViewAmount;
    private Utility utility;

    public AdapterDeposits(Context context, JSONArray deposits)
    {
        super();
        this.context = context;
        this.deposits = deposits;
        utility = new Utility();	   
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInflater = (LayoutInflater)  context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View adapter_charge = layoutInflater.inflate(R.layout.adapter_deposits_item, null);
        
        textViewCashRegister = (TextView) adapter_charge.findViewById(R.id.textViewCashRegister);
        textViewDate = (TextView) adapter_charge.findViewById(R.id.textViewDate);
        textViewAmount = (TextView) adapter_charge.findViewById(R.id.textViewAmount);

        JSONObject obj;
        
		try {
			obj = (JSONObject) deposits.get(i);
			
	        textViewCashRegister.setText(obj.getString(Keys.CASH_REGISTER));
	        String date = obj.getString(Keys.DATE);
	        String transformedDate = utility.transformDate(date);
	        textViewDate.setText(transformedDate);   
	        textViewAmount.setText(obj.getString(Keys.AMOUNT));
	        
		} catch (JSONException e) {
			e.printStackTrace();
		}
        return adapter_charge;
    }


    @Override
    public int getCount() {
        return deposits.length();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Object getItem(int i) {
    	
    	JSONObject jsonObj = null;
    	
        try {
        	jsonObj = (JSONObject) deposits.get(i);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        Object obj = (Object) jsonObj;
        		
		return obj;
    }

}
