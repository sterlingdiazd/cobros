package com.lynx.charges.adapters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lynx.charges.Keys;
import com.lynx.charges.R;
import com.lynx.charges.Utility;

public class AdapterChecks extends BaseAdapter{

    private Activity activity;
    private JSONArray checks;
    private TextView textViewCheck, textViewCashRegister, textViewDate, textViewBank, textViewAmount, textViewInvoice;
    private Utility utility;

    public AdapterChecks(Activity activity, JSONArray checks)
    {
        super();
        this.activity = activity;
        this.checks = checks;
        utility = new Utility();	   
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater layoutInflater = (LayoutInflater)  activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View adapter_charge = layoutInflater.inflate(R.layout.adapter_charge_item, null);
        
        textViewCheck = (TextView) adapter_charge.findViewById(R.id.textViewCheck);
        textViewCashRegister = (TextView) adapter_charge.findViewById(R.id.textViewCashRegister);
        textViewBank = (TextView) adapter_charge.findViewById(R.id.textViewBank);
        textViewAmount = (TextView) adapter_charge.findViewById(R.id.textViewAmount);
        textViewDate = (TextView) adapter_charge.findViewById(R.id.textViewDate);
        textViewInvoice = (TextView) adapter_charge.findViewById(R.id.textViewInvoice);
        
        JSONObject obj;
        
		try {
			obj = (JSONObject) checks.get(i);
			
	        textViewCheck.setText(obj.getString(Keys.CHECK_NO));
	        textViewCashRegister.setText(obj.getString(Keys.CASH_REGISTER));
	        String date = obj.getString(Keys.DATE);
	        String transformedDate = utility.transformDate(date);
	        textViewDate.setText(transformedDate);        
	        textViewBank.setText(obj.getString(Keys.BANK));
	        textViewAmount.setText(obj.getString(Keys.AMOUNT));
	        textViewInvoice.setText(obj.getString(Keys.INVOICE));
	        
		} catch (JSONException e) {
			e.printStackTrace();
		}
        return adapter_charge;
    }


    @Override
    public int getCount() {
        return checks.length();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public Object getItem(int i) {
    	
    	JSONObject jsonObj = null;
    	
        try {
        	jsonObj = (JSONObject) checks.get(i);
		} catch (JSONException e) {
			e.printStackTrace();
		}
        Object obj = (Object) jsonObj;
        		
		return obj;
    }



}
